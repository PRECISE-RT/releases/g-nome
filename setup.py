from distutils.core import setup
from Cython.Build import cythonize
import numpy

ext_module = cythonize("gnome/toarr.pyx")
ext_module[0].include_dirs = [numpy.get_include()]

setup(
      ext_modules=ext_module
)