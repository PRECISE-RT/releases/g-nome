import gnome
import sys
import getopt
import random
import tracemalloc


def main(argv):
    # snapshot1 = tracemalloc.take_snapshot()
    gTrackFile, save_dir, seed, radius, iterations, max_temp, cool_rate, occupancy_volume, logging_iteration, \
        model_name, print_structures, constrain_nucleus, save_models, two_phase_optimisation, move_exclusion = "", "", 0, 5.0, 1e6, 1.0, 0.0, 0.0, 0, "chrombuild", False, False, False, False, []

    try:
        opts, args = getopt.getopt(argv, "ho:s:r:n:t:c:v:l:m:g:",
                                   ["GTrackFile=", "SaveDir=", "Seed=", "Radius=", "Iterations=", "MaxTemp=", "CoolRate=", "OccupancyVolume=", "LoggingIteration=", "ModelName=", "PrintStructures", "ConstrainNucleus", "SaveModels", "TwoPhaseOpt", "MoveExclusion="])
    except getopt.GetoptError:
        print('gnome.py -g <gTrackFile> '
              '\nOPTIONAL FLAGS: -o <save_directory> -s <seed> -r <radius> -n <iterations>, -t <maxTemp> -c <coolRate>' '-v <occupancyVolume> -l <loggingIteration> -m <modelName> --PrintStructures --ConstrainNucleus --SaveModels --TwoPhaseOpt --MoveExclusion')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h' or opt == "--help":
            print('gnome.py -g <gTrackFile> '
                  '\nOPTIONAL FLAGS: -o <save_directory> -s <seed> -r <radius> -n <iterations>, -t <maxTemp> '
                  '-c <coolRate> -v <occupancyVolume> -l <loggingIteration> -m <modelName> --PrintStructures --ConstrainNucleus --SaveModels --TwoPhaseOpt --MoveExclusion')
            sys.exit()
        elif opt in ("-g", "--GTrackFile"):
            gTrackFile = arg
        elif opt in ("-o", "--SaveDir"):
            save_dir = arg
        elif opt in ("-s", "--Seed"):
            seed = int(arg)
        elif opt in ("-r", "--Radius"):
            # can be float or tuple for spheroid or ellipsoid respectively
            radius = eval(arg)
        elif opt in ("-n", "--Iterations"):
            # can be int of tuple for single or two phase optimisation respectively
            iterations = eval(arg)
        elif opt in ("-t", "--MaxTemp"):
            max_temp = float(arg)
        elif opt in ("-c", "--CoolRate"):
            cool_rate = float(arg)
        elif opt in ("-v", "--OccupancyVolume"):
            occupancy_volume = float(arg)
        elif opt in ("-l", "--LoggingIteration"):
            logging_iteration = int(arg)
        elif opt in ("-m", "--ModelName"):
            model_name = arg
        elif opt in "--PrintStructures":
            print_structures = True
        elif opt in "--ConstrainNucleus":
            constrain_nucleus = True
        elif opt in "--TwoPhaseOpt":
            if type(iterations) != tuple:
                sys.exit(
                    "In order to use --TwoPhaseOpt you need to set both iteration values e.g. -n 100,200")
            two_phase_optimisation = True
        elif opt in "--MoveExclusion":
            move_exclusion.append(arg)

    if len(opts) < 1:
        print('gnome.py -g <gTrackFile> [NOTE: AT LEAST ONE PARAMETER IS NEEDED]'
              '\nOPTIONAL FLAGS: -o <save_directory> -s <seed> -r <radius> -n <iterations>, -t <maxTemp> '
              '-c <coolRate> -v <occupancyVolume> -l <loggingIteration> -m <modelName> --PrintStructures --ConstrainNucleus --MoveExclusion')
        sys.exit()

    rng = random.Random()
    rng.seed(seed)
    print(f"Run seed: {seed}")
    model = gnome.Model.Model(model_name, rng)
    model.seed = seed
    model.set_nucleus_radius(radius)
    gft2 = gnome.Files.GtrackFile()
    gft2.read_gtrack(gTrackFile)

    if occupancy_volume > 0:
        gft2.parse_gtack(model, scale_bead_size=True,
                         nuclear_occupancy=occupancy_volume)
    else:
        gft2.parse_gtack(model)

    print("Parsed GTrack successfully")

    if constrain_nucleus is True:
        model.add_nucleus_constraints()
        model.score = model.get_loss_score()  # updated loss score for new constraints
    if "crankshaft" not in move_exclusion or "CrankShaft" not in move_exclusion:
        model.add_randomiser(gnome.Randomiser.CrankShaftRandomiser())
    if "microcrank" not in move_exclusion or "MicroCrank" not in move_exclusion:
        model.add_randomiser(gnome.Randomiser.MicroCrankShaftRandomiser())
    if "armwiggle" not in move_exclusion or "ArmWiggle" not in move_exclusion:
        model.add_randomiser(gnome.Randomiser.ArmWiggleRandomiser())
    if "armrotate" not in move_exclusion or "ArmRotate" not in move_exclusion:
        model.add_randomiser(gnome.Randomiser.ArmRotateRandomiser())
    if "translate" not in move_exclusion or "Translate" not in move_exclusion:
        model.add_randomiser(gnome.Randomiser.TranslateRandomiser(1.0))
    if "rotate" not in move_exclusion or "Rotate" not in move_exclusion:
        model.add_randomiser(gnome.Randomiser.RotationRandomiser())

    mcmc = gnome.MarkovChain.MCMC(model)

    if logging_iteration == 0:
        logging_iteration = iterations+1
    else:
        print(
            f"Logging loss score at frequency {logging_iteration} iterations: \nIteration, loss score")

    # Single phase optimisation routine (INTERACTIONS+NUCLEUS)
    if two_phase_optimisation is False:
        iterations = int(iterations)
        for i in range(0, iterations):
            max_temp *= (1 - cool_rate)
            success = mcmc.optimisation_step(temp=max_temp)
            assert success
            if i % logging_iteration == 0 and print_structures is True:
                if save_dir != "":
                    gnome.Files.CMMFile().write_cmm(
                        model, fp=f'{save_dir}/{model_name}_iter_{i}.cmm')
                else:
                    gnome.Files.CMMFile().write_cmm(
                        model, fp=f'{model_name}_iter_{i}.cmm')
            if i % logging_iteration == 0 and logging_iteration != iterations+1:
                model.record_scores()
                print(f"{i}, {model.score}")

    # Two phase optimisation routine: 1) ITERACTIONS 2) ITERACTIONS+NUCLEUS
    elif two_phase_optimisation is True:
        model.change_constraint_state('NUCLEUS', active=False)
        model.score = model.get_loss_score()  # updated loss score for new constraints
        for i in range(0, iterations[0]):
            max_temp *= (1 - cool_rate)
            success = mcmc.optimisation_step(temp=max_temp)
            assert success
            if i % logging_iteration == 0 and print_structures is True:
                if save_dir != "":
                    gnome.Files.CMMFile().write_cmm(
                        model, fp=f'{save_dir}/{model_name}_iter_{i}.cmm')
                else:
                    gnome.Files.CMMFile().write_cmm(
                        model, fp=f'{model_name}_iter_{i}.cmm')
            if i % logging_iteration == 0 and logging_iteration != iterations[0]+1:
                model.record_scores()
                print(f"{i}, {model.score}")
        model.change_constraint_state('NUCLEUS', active=True)
        model.score = model.get_loss_score()  # updated loss score for new constraints
        for i in range(0, iterations[1]):
            max_temp *= (1 - cool_rate)
            success = mcmc.optimisation_step(temp=max_temp)
            assert success
            if i % logging_iteration == 0 and print_structures is True:
                if save_dir != "":
                    gnome.Files.CMMFile().write_cmm(
                        model, fp=f'{save_dir}/{model_name}_iter_{i}.cmm')
                else:
                    gnome.Files.CMMFile().write_cmm(
                        model, fp=f'{model_name}_iter_{i}.cmm')
            if i % logging_iteration == 0 and logging_iteration != iterations[1]+1:
                model.record_scores()
                print(f"{i}, {model.score}")

        # # DEBUG ###################################################
        # if i % 100 == 0:
        #     snapshot2 = tracemalloc.take_snapshot()
        #     top_stats = snapshot2.compare_to(snapshot1, 'lineno')
        #     print("[ Top 10 differences ]")
        #     for stat in top_stats[:10]:
        #         print(stat)
        #     snapshot1 = snapshot2
        # ##########################################################

    if save_dir != "":
        gnome.Files.CMMFile().write_cmm(
            model, fp=f'{save_dir}/{model_name}.cmm')
        gnome.Files.VerticiesFile().write_vertices(model, gTrackFile, occupancy_volume, iterations, logging_iteration,
                                                        constrain_nucleus, max_temp, cool_rate, move_exclusion, two_phase_optimisation, fp=f'{save_dir}/{model_name}.vert.txt')
    else:
        # will save as default "model_name.cmm"
        gnome.Files.CMMFile().write_cmm(model, fp=f'{model_name}.cmm')
        gnome.Files.VerticiesFile().write_vertices(model, gTrackFile, occupancy_volume, iterations, logging_iteration,
                                                        constrain_nucleus, max_temp, cool_rate, move_exclusion, two_phase_optimisation, fp=f'{model_name}.vert.txt')


if __name__ == "__main__":
    # tracemalloc.start()
    main(sys.argv[1:])
