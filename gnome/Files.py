import sys
import os
import datetime
import subprocess

from . import Chromosome, Bead, Model


class GtrackFile(object):

    def __init__(self):
        self.header = None
        self.chromosomes = {}
        self.bead_id_chr = {}
        self.periphery_ids = []
        self.center_ids = []
        self.interaction_ids = []
        self.interaction_distance_ids = []
        self.interaction_lower_distance_ids = []
        self.interaction_upper_distance_ids = []
        self.dist_info = {}
        self.weight_info = {}
        self.periphery_weight = {}
        self.center_weight = {}
        self.bead_radius = 0.5  # default value
        self.has_periphery = False
        self.has_edges = False
        self.has_colour = False
        self.has_bead_radius = False
        self.max_radius = 0

    def read_gtrack(self, fp: str):
        f = open(fp, "r")
        data_set = {}
        for i, line in enumerate(f.readlines()):
            if line.startswith("###"):
                self.header = line.replace('###', '').replace('\n', '').split('\t')
                self.has_periphery = "periphery" in self.header
                self.has_edges = "edges" in self.header
                self.has_colour = "color" in self.header or "colour" in self.header
                self.has_bead_radius = "radius" in self.header
                data_set = {head: [] for head in self.header}
                continue
            elif line.startswith("#"):  # comments
                continue

            for header, data in zip(self.header, line.replace('###', '').replace('\n', '').split('\t')):
                data_set[header].append(data)

            # filling in this information here to save iterating through it later
            # periphery
            if self.has_periphery and data_set['periphery'][-1] != '.':
                if data_set['periphery'][-1] == '1':
                    self.periphery_ids.append(data_set['id'][-1])
                elif data_set['periphery'][-1] == '0':
                    self.center_ids.append(data_set['id'][-1])
                elif ',' in data_set['periphery'][-1]:  # has periphery weighting
                    if data_set['periphery'].split(',')[0] == '1':
                        self.periphery_ids.append(data_set['id'][-1])
                        self.periphery_weight[self.periphery_ids[-1]] = float(data_set['periphery'].split(',')[1])
                    elif data_set['periphery'].split(',')[0] == '0':
                        self.center_ids.append(data_set['id'][-1])
                        self.center_weight[self.center_ids[-1]] = float(data_set['periphery'].split(',')[1])
                    else:
                        sys.exit(f'Gtrack Line Error {i}: Periphery can be either 0 or 1, when a weight is given.')

            # edges
            if self.has_edges and data_set['edges'][-1] != '.':
                edge_list = data_set['edges'][-1].split(';')
                for edge in edge_list:
                    if edge == data_set['id'][-1] or edge.split('=')[0] == data_set['id'][-1]:
                        sys.exit(f'Gtrack Line Error {i}: Same edge value as own BeadID.')
                    elif '=' not in edge:
                        id_pair = (data_set['id'][-1], edge)
                        if id_pair not in self.interaction_ids:
                            self.interaction_ids.append(id_pair)
                    else:
                        edge_id = edge.split('=')[0]
                        edge_weight = edge.split('=')[1]
                        id_pair = (data_set['id'][-1], edge_id)
                        if '.' not in edge_weight:  # No weighting actually applied
                            self.interaction_ids.append(id_pair)
                        else:
                            edge_details = edge_weight.split(',')
                            if edge_details[2] not in ["0", "1", "."]:
                                sys.exit(f'Gtrack Line Error {i}: Boundary information value can only be 0, 1 or .')
                            elif float(edge_details[0]) < 0 or float(edge_details[1]) < 0:
                                sys.exit(f'Gtrack Line Error {i}: Weight and Distance should be greater than 0')
                            elif edge_details[2] == '0':
                                self.interaction_upper_distance_ids.append(id_pair)
                            elif edge_details[2] == '1':
                                self.interaction_lower_distance_ids.append(id_pair)
                            elif edge_details[2] == '.':
                                self.interaction_distance_ids.append(id_pair)
                            self.weight_info[id_pair] = float(edge_details[0])
                            self.dist_info[id_pair] = float(edge_details[1])

        # Setup chromosomes
        self.chromosomes = {chr_name: Chromosome.Chromosome(chr_name) for chr_name in sorted(set(data_set['seqid']))}
        # setup beads
        bead_store = {chr_name: [] for chr_name in sorted(set(data_set['seqid']))}

        for i, (chr, start, end, radius, id) in enumerate(zip(data_set['seqid'], data_set['start'], data_set['end'],
                                                              data_set['radius'], data_set['id'])):
            bead = Bead.Bead()
            bead.initialize(int(start), int(end), float(radius), id)
            bead.set_coordinates(0, 0, 0)  # These will get randomly set at start of simulation
            if float(radius) > self.max_radius:
                self.max_radius = float(radius)
            if self.has_colour:
                try:
                    colour = data_set['color'][i].split(',')
                except KeyError:
                    colour = data_set['colour'][i].split(',')
                bead.set_colour(float(colour[0]), float(colour[1]), float(colour[2]))
            bead_store[chr].append(bead)

            self.bead_id_chr[id] = chr

        # Adding the bead to chromosome
        for chrom in self.chromosomes:
            for bead in bead_store[self.chromosomes[chrom].name]:
                self.chromosomes[chrom].add_bead(bead)

        print(f"Gtrack file \"{fp.split('/')[-1]}\" read successfully")

    def parse_gtack(self, model: Model, fp=None, scale_bead_size=False, nuclear_occupancy=0.2):
        if fp is not None:
            self.read_gtrack(fp)
        # Adding chromosomes to model
        for chrom in self.chromosomes:
            if self.has_colour is False:
                red = model.rng.uniform(0, 1)
                green = model.rng.uniform(0, 1)
                blue = model.rng.uniform(0, 1)
                self.chromosomes[chrom].set_colour(red, green, blue)
            model.add_chromosome(self.chromosomes[chrom])
        # Scaling bead-sizes based on nuclear occupancy
        if scale_bead_size:
            previous_scale = model.get_nuclear_occupancy()
            model.rescale_bead_size(nuclear_occupancy)
            print(f'Rescaled bead size from {previous_scale:.3f} to {model.get_nuclear_occupancy():.3f}')
        # Adding interactions
        for interactions in self.interaction_ids:
            if interactions in self.weight_info:
                weight = self.weight_info[interactions]
            else:
                weight = 1.0
            if self.bead_id_chr[interactions[0]] == self.bead_id_chr[interactions[1]]:
                model.add_interaction_constraint(interactions[0], interactions[1], weight, 'INTERACTION_INTRA')
            else:
                model.add_interaction_constraint(interactions[0], interactions[1], weight, 'INTERACTION_INTER')
        # Adding non-bounded distances
        for interactions in self.interaction_distance_ids:
            model.add_interaction_distance_constraint(interactions[0], interactions[1],
                                                      self.dist_info[interactions],
                                                      self.weight_info[interactions])
        # Adding lower-bound distances
        for interactions in self.interaction_lower_distance_ids:
            model.add_interaction_lower_distance_constraint(interactions[0], interactions[1],
                                                            self.dist_info[interactions],
                                                            self.weight_info[interactions])
        # Adding upper-bound distances
        for interactions in self.interaction_upper_distance_ids:
            model.add_interaction_upper_distance_constrain(interactions[0], interactions[1],
                                                           self.dist_info[interactions],
                                                           self.weight_info[interactions])
        # Adding periphery
        for periphery_id in self.periphery_ids:
            if periphery_id in self.periphery_weight:
                weight = self.periphery_weight[periphery_id]
            else:
                weight = 1.0
            model.add_periphery_constraint(periphery_id, weight)
        # adding center constraint
        for center_id in self.center_ids:
            if center_id in self.center_weight:
                weight = self.center_weight[center_id]
            else:
                weight = 1.0
            model.add_center_constraint(center_id, weight)
        # Checking there is symmetry and all interactions have been specified twice
        if not (model.interaction_specified_symmetric(self.interaction_ids) or
                model.interaction_specified_symmetric(self.interaction_distance_ids) or
                model.interaction_specified_symmetric(self.interaction_lower_distance_ids) or
                model.interaction_specified_symmetric(self.interaction_upper_distance_ids)):
            sys.exit('Advanced option: Edge column is not symmetrical for one or more beads. Interacting beads have to '
                     'be specified twice in the corresponding edge column with the same information.')

        if not model.interaction_weights_specified_symmetric(self.dist_info):
            sys.exit('Advanced option: Optimal distance specified is not the same for interacting beads. '
                     'Please verify the edge column.')

        if not model.interaction_weights_specified_symmetric(self.weight_info):
            sys.exit('Advanced option: Weight specified is not the same for interacting beads. '
                     'Please verify the edge column.')
        model.max_radius = self.max_radius
        # Initialise and setup structures
        model.reset_all_chromosomes()
        print(f"# chromosomes: {len(self.chromosomes)}")
        print(f"# nuclear radius: {model.nucleus_radius}")
        print(f"# beads: {model.get_number_of_beads()}")
        print(f"# interactions: {len(self.interaction_ids) / 2}")
        print(f"# interactions with given weight: {len(self.weight_info) / 2}")
        print(f"# non-bounded interactions with given distances: {len(self.interaction_distance_ids) / 2}")
        print(f"# upper-bounded interactions with given distances: {len(self.interaction_upper_distance_ids) / 2}")
        print(f"# lower-bounded interactions with given distances: {len(self.interaction_lower_distance_ids) / 2}")
        print(f"# periphery beads: {len(self.periphery_ids)}")
        print(f"# periphery beads with given weight: {len(self.periphery_weight)}")
        print(f"# center beads: {len(self.center_ids)}")
        print(f"# center beads with given weight: {len(self.center_weight)}")

    def write_gtack(self, fp: str):
        pass


class CMMFile(object):

    def __init__(self):
        pass

    def read_cmm(self):
        pass

    def write_cmm(self, model: Model.Model, fp="output.cmm"):
        try:
            f = open(fp, "w")
        except FileNotFoundError:
            os.mkdir(fp)
            f = open(fp, "w")

        smallest_bead = float('inf')

        for chromosome in model.chromosomes:
            for bead in chromosome:
                if bead.radius < smallest_bead:
                    smallest_bead = bead.radius
        link_radius = smallest_bead * 0.5

        f.write(f"<marker_set name=\"{model.name}\">\n")
        counter = 0
        prev_chrom = None
        prev_id = None
        for chromosome in model.chromosomes:
            for bead in chromosome:
                f.write(f"<marker id=\"{counter}\" x=\"{bead.x}\" y=\"{bead.y}\" z=\"{bead.z}\" radius=\"{bead.radius}\" r=\"{bead.colour[0]}\" g=\"{bead.colour[1]}\" b=\"{bead.colour[2]}\" chrID=\"{chromosome.name}\" beadID=\"{bead.id}\"/>\n")
                if prev_chrom == chromosome.name:
                    f.write(f"<link id1=\"{prev_id}\" id2=\"{counter}\" r=\"{bead.colour[0]}\" g=\"{bead.colour[1]}\" b=\"{bead.colour[2]}\" radius=\"{link_radius}\"/>\n")
                prev_chrom = chromosome.name
                prev_id = counter
                counter += 1
        f.write("</marker_set>\n")
        f.close()


class VerticiesFile(object):

    def __init__(self):
        pass

    def read_vertices(self, fp):
        f = open(fp, 'r') 
        data = f.readlines()
        results = {"Header": {}, "Data": {}}
        for i, line in enumerate(data):
            if line.startswith("# CostRecords:"):
                tag = line.split(" ")[1].strip(": ")
                value = line.split(":")[-1].strip(" []\n").split(",")
                value = [eval(idx) for idx in value]
                results["Header"][tag] = value
            elif line.startswith("#"):
                tag = line.split(" ")[1].strip(": ")
                value = line.split(": ")[-1].strip("\n")
                results["Header"][tag] = value
            else:
                results['Data'][f'BeadID_{i}'] = {}
                values = line.split(" ")
                results['Data'][f'BeadID_{i}']['Chromosome'] = values[0]
                results['Data'][f'BeadID_{i}']['Copy'] = values[1]
                results['Data'][f'BeadID_{i}']['X'] = values[2]
                results['Data'][f'BeadID_{i}']['Y'] = values[3]
                results['Data'][f'BeadID_{i}']['Z'] = values[4]
                results['Data'][f'BeadID_{i}']['Radius'] = values[5]
                results['Data'][f'BeadID_{i}']['Size'] = values[6]
        return results

    def calculate_rescale_factor(self, model: Model.Model, occupancy_volume):
        if type(model.nucleus_radius) is float:
            return model.nucleus_radius*(occupancy_volume**(1/3))
        elif type(model.nucleus_radius) is tuple:
            radius = ((model.nucleus_radius[0] * model.nucleus_radius[1] * model.nucleus_radius[2]) ** (1/3))
            return radius*(occupancy_volume**(1/3))

    def write_vertices(self, model: Model.Model, gTrackFile, occupancy_volume, iterations, logging_iteration, constrain_nucleus, max_temp, cool_rate, move_exclusion, two_phase_optimisation, fp="output.cmm"):
        try:
            f = open(fp, "w")
        except FileNotFoundError:
            os.mkdir(fp)
            f = open(fp, "w")
        # Write header
        f.write(f"# Created: {datetime.datetime.now()}\n")
        try:
            f.write(f"# SolverGitURL: {subprocess.check_output(['git', 'config', '--get', 'remote.origin.url']).strip().decode('utf-8')}\n")
            f.write(f"# SolverGitBranch: {subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip().decode('utf-8')}\n")
            f.write(f"# SolverGitHash: {subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip().decode('utf-8')}\n")
        except:
            f.write(f"# SolverGitURL: Not Found\n")
            f.write(f"# SolverGitBranch: Not Found\n")
            f.write(f"# SolverGitHash: Not Found\n")
        f.write(f"# ModelName: {model.name}\n")
        f.write(f"# GTrackFile: {gTrackFile}\n")
        f.write(f"# Seed: {model.seed}\n")
        f.write(f"# Radius: {model.nucleus_radius}\n")
        f.write(f"# OccupancyFactor: {occupancy_volume}\n")
        f.write(f"# RescaleFactor: {self.calculate_rescale_factor(model, occupancy_volume)}\n")
        f.write(f"# Iterations: {iterations}\n")
        f.write(f"# LoggingInteration: {logging_iteration}\n")
        f.write(f"# CostRecords: {model.recorded_scores}\n")
        f.write(f"# ConstrainNucleus: {constrain_nucleus}\n")
        f.write(f"# TwoPhaseOptimisation: {two_phase_optimisation}\n")
        f.write(f"# MaxTemp: {max_temp}\n")
        f.write(f"# CoolRate: {cool_rate}\n")
        f.write(f"# MovesExcluded: {move_exclusion}\n")
        f.write(f"# Chromosomes: {len(model.chromosomes)}\n")
        f.write(f"# Beads: {model.get_number_of_beads()}\n")
        f.write(f"# Constraints: {len(model.constraints)}\n")

        # Write verticies data
        for chromosome in model.chromosomes:
            for bead in chromosome:
                chrom_name_split = chromosome.name.split("_")
                chrom_position_start = int(bead.id.split(":")[1].split("-")[0])
                chrom_position_end = int(bead.id.split(":")[1].split("-")[1])
                f.write(f"{chrom_name_split[0]} {chrom_name_split[1]} {bead.x} {bead.y} {bead.z} {bead.radius} {chrom_position_end-chrom_position_start}\n")
        f.close()
