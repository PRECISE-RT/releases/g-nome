import math

from . import Model


class MCMC(object):

    def __init__(self, model: Model.Model):
        self.model = model
        self.failed = 0

    def optimisation_step(self, temp=1, max_attempts=10000):
        counter = 0
        success = False
        while counter < max_attempts and success is False:
            accept = True
            proposed_move = self.model.propose_move(max_attempts=1)
            # Couldn't propose move with given randomiser
            if proposed_move is None:
                counter += 1
                continue

            before = self.model.get_partial_loss_score(proposed_move.chromosome, proposed_move.start,
                                                       proposed_move.stop)
            old_coords = self.model.accept_move(proposed_move)
            after = self.model.get_partial_loss_score(proposed_move.chromosome, proposed_move.start, proposed_move.stop)

            if after > before:
                p_accept = math.exp((before - after) / temp)
                accept = self.model.rng.uniform(0, 1) < p_accept

                if accept is False:
                    self.failed += 1
                    self.model.accept_move(old_coords)
            if accept is True:
                self.model.score += after - before

            success = True
        return success
