import bisect
import collections
import random
import sys
import pickle
from itertools import accumulate

import numpy as np
# from numba.typed import List

from . import Bead, Chromosome, Constraint, Randomiser, Util, toarr


class Model(object):

    def __init__(self, model_name: str, rng: random.Random):

        self.name = model_name
        self.rng = rng
        self.seed = None
        self.center_bead = Bead.Bead()
        self.center_bead.initialize(0, 1, 0.2, 'center')
        self.center_bead.set_coordinates(0.0, 0.0, 0.0)
        self.has_nucleus = False
        self.chromosomes = []
        self.nucleus_radius = None
        self.get_bead = {}
        self.constraints = {}
        self.randomisers = []
        self.randomiser_weights = []
        self.score = 0
        self.recorded_scores = []
        self.max_radius = 0
        self.clash_failures = 0
        self.intra_clash_failures = 0

    def update_bead_map(self):
        self.get_bead.clear()
        for chromosome in self.chromosomes:
            for bead in chromosome:
                self.get_bead[bead.id] = bead

    def add_chromosome(self, chromosome: Chromosome):
        self.chromosomes.append(chromosome)  # self.chromosomes is a set and therefore should be unique
        self.update_bead_map()

    def add_constraint(self, *args, constraint_type="Regular", spring_constant=1.0, lower_bound=0.0,
                       upper_bound=float("inf")):
        if len(args) == 1:  # If parsed a constraint directly
            bead_ids = args[0].get_bead_ids()
            try:
                self.constraints[bead_ids[0].id].append(args[0])
            except KeyError:
                self.constraints[bead_ids[0].id] = []
                self.constraints[bead_ids[0].id].append(args[0])
        else:  # if parsed beadid1, beadid2, targetdistance, constraintType, springConstant, lowerbound, upperbound
            assert (args[0] in self.get_bead)
            assert (args[1] in self.get_bead)
            b1 = self.get_bead[args[0]]
            b2 = self.get_bead[args[1]]
            self.add_constraint(
                Constraint.Constraint(b1, b2, args[2], constraint_type, spring_constant, lower_bound, upper_bound))

    def add_interaction_constraint(self, bead_id1: str, bead_id2: str, spring_constant: float, interaction_type: str):
        assert (bead_id1 in self.get_bead)
        assert (bead_id2 in self.get_bead)
        assert (
                    interaction_type == "INTERACTION" or interaction_type == "INTERACTION_INTRA" or interaction_type == "INTERACTION_INTER")
        b1 = self.get_bead[bead_id1]
        b2 = self.get_bead[bead_id2]
        self.add_constraint(bead_id1, bead_id2, b1.radius + b2.radius, constraint_type=interaction_type,
                            spring_constant=spring_constant)

    def add_interaction_distance_constraint(self, bead_id1: str, bead_id2: str, bead_pair_distance: float,
                                            spring_constant: float):
        assert (bead_id1 in self.get_bead)
        assert (bead_id2 in self.get_bead)
        b1 = self.get_bead[bead_id1]
        b2 = self.get_bead[bead_id2]
        self.add_constraint(bead_id1, bead_id2, b1.radius + b2.radius + bead_pair_distance,
                            constraint_type="INTERACTION_DIST", spring_constant=spring_constant)

    def add_interaction_lower_distance_constraint(self, bead_id1, bead_id2, bead_pair_distance, spring_constant):
        assert (bead_id1 in self.get_bead)
        assert (bead_id2 in self.get_bead)
        b1 = self.get_bead[bead_id1]
        b2 = self.get_bead[bead_id2]
        self.add_constraint(bead_id1, bead_id2, b1.radius + b2.radius + bead_pair_distance,
                            constraint_type="INTERACTION_DIST", spring_constant=spring_constant, lower_bound=0,
                            upper_bound=b1.radius + b2.radius + bead_pair_distance)

    def add_interaction_upper_distance_constrain(self, bead_id1, bead_id2, bead_pair_distance, spring_constant):
        assert (bead_id1 in self.get_bead)
        assert (bead_id2 in self.get_bead)
        b1 = self.get_bead[bead_id1]
        b2 = self.get_bead[bead_id2]
        self.add_constraint(bead_id1, bead_id2, b1.radius + b2.radius + bead_pair_distance,
                            constraint_type="INTERACTION_DIST", spring_constant=spring_constant,
                            lower_bound=b1.radius + b2.radius + bead_pair_distance)

    def add_boundary_constraint(self, bead_id, boundary_radius, spring_constant):
        assert (bead_id in self.get_bead)
        b1 = self.get_bead[bead_id]
        self.add_constraint(
            Constraint.Constraint(b1, self.center_bead, boundary_radius - b1.radius, constraint_type="BOUNDARY",
                                  spring_constant=spring_constant, lower_bound=boundary_radius - b1.radius))

    def add_nucleus_constraint(self, bead_id, spring_constant):
        assert (bead_id in self.get_bead)
        assert self.has_nucleus
        b1 = self.get_bead[bead_id]
        self.add_constraint(
            Constraint.Constraint(b1, self.center_bead, self.nucleus_radius, constraint_type="NUCLEUS",
                                  spring_constant=spring_constant))

    def add_periphery_constraint(self, bead_id, spring_constant):
        assert (bead_id in self.get_bead)
        assert self.has_nucleus
        b1 = self.get_bead[bead_id]
        self.add_constraint(
            Constraint.Constraint(b1, self.center_bead, self.nucleus_radius, constraint_type="PERIPHERY",
                                  spring_constant=spring_constant))

    def add_center_constraint(self, bead_id, spring_constant):
        assert (bead_id in self.get_bead)
        b1 = self.get_bead[bead_id]
        self.add_constraint(
            Constraint.Constraint(b1, self.center_bead, 0, constraint_type="CENTER", spring_constant=spring_constant))

    def set_nucleus_radius(self, radius):
        if type(radius) is float:
            assert (radius > 0)
        else:
            assert type(radius) is tuple
            for val in radius:
                assert (val > 0)
        self.nucleus_radius = radius
        self.has_nucleus = True

    def change_constraint_state(self, constraint_type, active: bool):
        # Turn constraints types on and off
        for entry in self.constraints:
            for constraint in self.constraints[entry]:
                if constraint.constraint_type == constraint_type:
                    constraint.active = active

    def re_weight_constraints(self, constraint_type, new_spring_constant):
        for entry in self.constraints:
            for constraint in self.constraints[entry]:
                if constraint.constraint_type == constraint_type:
                    constraint.k = new_spring_constant

    def get_loss_score(self, *args):
        # assert len(args) > 1
        if len(args) == 0:
            res = 0
            for bead_id in self.constraints:
                for constraint in self.constraints[bead_id]:
                    if constraint.active is False:
                        continue
                    ct = constraint.constraint_type
                    if ct == "INTERACTION" or ct == "INTERACTION_DIST" or ct == "INTERACTION_INTRA" or ct == "INTERACTION_INTER":
                        res += constraint.evaluate() / 2
                    else:
                        res += constraint.evaluate()
            return res
        else:
            res = 0
            for constraint in self.constraints:
                if constraint.active is False:
                    continue
                if constraint.constraint_type == args[0]:
                    ct = constraint.constraint_type
                    if ct == "INTERACTION" or ct == "INTERACTION_DIST" or ct == "INTERACTION_INTRA" or ct == "INTERACTION_INTER":
                        res += constraint.evaluate() / 2
                    else:
                        res += constraint.evaluate()
            return res

    def get_partial_loss_score(self, chromosome: Chromosome, start_position: int, end_position: int):
        res = 0
        bead_iter = iter(chromosome[start_position: end_position+1])
        move_set = set()
        [move_set.add(bead.id) for bead in chromosome[start_position: end_position+1]]
        for bead in bead_iter:
            if bead.id in self.constraints: # Catches the beads without any constraints
                for constraint in self.constraints[bead.id]:
                    if constraint.active is False:
                        continue
                    if constraint.bead1.id in move_set and \
                            constraint.bead2.id in move_set:
                        res += constraint.evaluate()/2
                    else:
                        res += constraint.evaluate()
        return res

    def record_scores(self):
        self.recorded_scores.append(self.score)

    def add_randomiser(self, randomiser: Randomiser, weight=1.0):
        self.randomisers.append(randomiser)
        self.randomiser_weights.append(weight)

    def remove_all_randomiser(self):
        self.randomisers.clear()
        self.randomiser_weights.clear()

    def weighted_sample_pos(self, randomiser_weights: list):
        weight_sum = sum(randomiser_weights)
        randomiser_weights = list(accumulate(randomiser_weights))
        random_weighting = self.rng.uniform(0.0, weight_sum)
        pos = bisect.bisect_left(randomiser_weights, random_weighting)
        return pos

    def propose_move(self, max_attempts=10000):
        assert len(self.randomisers) > 0
        i_rand = self.weighted_sample_pos(self.randomiser_weights)
        # i_rand = self.rng.randint(0, len(self.randomisers)-1)
        i_chr = self.rng.randint(0, len(self.chromosomes) - 1)
        for i in range(0, max_attempts):
            mov = self.randomisers[i_rand].randomise(self.rng, chrom=self.chromosomes[i_chr])
            if mov.mat is not None:
                mov.validate()
                if self.is_clashing(mov) is False:
                    return mov
        return None

    def is_clashing(self, move: Randomiser.Move, only_intra=False):
        # Intrachromosomal
        beads_to_evaluate = len(move.chromosome) - (move.stop - move.start) - 1
        if beads_to_evaluate != 0:  # Otherwise this is entire chromosome so no intrachromosomal evaluation needed
            beads_before = [[bead.x, bead.y, bead.z, bead.radius] for bead in move.chromosome[:move.start]]
            beads_after = [[bead.x, bead.y, bead.z, bead.radius] for bead in move.chromosome[move.stop+1:]]
            # numba_list = List()
            # [numba_list.append([x, y, z, r]) for x, y, z, r in (beads_before+beads_after)]
            # bead_details = Util.to_numpy_array(numba_list)
            bead_details = toarr.toarray(beads_before+beads_after)
            move_radius = np.array([bead.radius for bead in move.chromosome[move.start:move.stop+1]])
            if toarr.bead_coordinate_clash(np.array(move.mat), bead_details[:, 0:3], move_radius, bead_details[:, 3]):
                self.intra_clash_failures += 1
                return True
        if only_intra:
            return False
        # Interchromosomal
        move_radius = np.array([bead.radius for bead in move.chromosome[move.start:move.stop + 1]])
        max_move_radius = np.max(move_radius)
        mat = np.array(move.mat)
        max_x = np.max(mat[:, 0])+max_move_radius+self.max_radius
        max_y = np.max(mat[:, 1])+max_move_radius+self.max_radius
        max_z = np.max(mat[:, 2])+max_move_radius+self.max_radius
        min_x = np.min(mat[:, 0])-max_move_radius-self.max_radius
        min_y = np.min(mat[:, 1])-max_move_radius-self.max_radius
        min_z = np.min(mat[:, 2])-max_move_radius-self.max_radius
        for chromosome in self.chromosomes:  # flatten
            if chromosome != move.chromosome:
                f = [(bead.x, bead.y, bead.z, bead.radius-0.00000001) for bead in chromosome if
                                              max_x > bead.x > min_x and
                                              max_y > bead.y > min_y and
                                              max_z > bead.z > min_z]
                if len(f) == 0:
                    continue
                # numba_list = List()
                # [numba_list.append([x, y, z, r]) for x, y, z, r in f]
                # bead_details = Util.to_numpy_array(numba_list)
                bead_details = toarr.toarray(f)
                if toarr.bead_coordinate_clash(mat, bead_details[:, 0:3], move_radius, bead_details[:, 3]):
                    self.clash_failures += 1
                    return True
        return False

    def accept_move(self, move: Randomiser.Move):
        old_mat = np.empty((len(move.mat), 3))
        for i, row in enumerate(move.mat):
            old_mat[i, 0] = move.chromosome[move.start+i].x
            old_mat[i, 1] = move.chromosome[move.start+i].y
            old_mat[i, 2] = move.chromosome[move.start+i].z
            move.chromosome[move.start+i].set_coordinates(row[0], row[1], row[2])
        old_structure = Randomiser.Move(move.chromosome, move.start, move.stop, old_mat)
        return old_structure

    def get_total_chr_length(self):
        res = 0
        for chromosome in self.chromosomes:
            for bead in chromosome:
                res += bead.radius * 2
        return res

    def get_number_of_beads(self):
        res = 0
        for chromosome in self.chromosomes:
            res += len(chromosome)
        return res

    def number_of_chromosomes(self):
        return len(self.chromosomes)

    def reset_all_chromosomes(self, max_attempts=1000):
        limit = (self.get_total_chr_length() / self.number_of_chromosomes()) * 0.1
        for chromosome in self.chromosomes:
            radii = []
            for bead in chromosome:
                radii.append(bead.radius)
            for i in range(0, max_attempts):
                origin_x = self.rng.uniform(-limit, limit)
                origin_y = self.rng.uniform(-limit, limit)
                origin_z = self.rng.uniform(-limit, limit)
                call_seed = self.rng.randint(0, 1e10)
                mat = Util.create_self_avoiding_walk(np.array(radii),
                                                     np.array([origin_x, origin_y, origin_z]),
                                                     call_seed, max_iterations=max_attempts)
                if mat is not None:
                    mov = Randomiser.Move(chromosome, start=0, stop=len(chromosome) - 1, mat=mat)
                    mov.validate()
                    if self.is_clashing(mov) is False:
                        self.accept_move(mov)
                        break
        self.score = self.get_loss_score()

    def add_nucleus_constraints(self, spring_constant=1):
        assert self.has_nucleus
        for chromosome in self.chromosomes:
            for bead in chromosome:
                self.add_nucleus_constraint(bead.id, spring_constant)

    def add_boundary_constraints(self, spring_constant=1):
        assert self.has_nucleus
        for chromosome in self.chromosomes:
            for bead in chromosome:
                self.add_boundary_constraint(bead.id, self.nucleus_radius, spring_constant)

    def add_center_constraints(self, spring_constant=1):
        assert self.has_nucleus
        for chromosome in self.chromosomes:
            for bead in chromosome:
                self.add_center_constraint(bead.id, spring_constant)

    def calculate_constraint_scores(self, constraint_type):
        for entry in self.constraints:
            for constraint in self.constraints[entry]:
                if constraint.constraint_type == constraint_type:
                    ct = constraint.constraint_type
                    if ct == "INTERACTION" or ct == "INTERACTION_DIST" or ct == "INTERACTION_INTRA" or ct == "INTERACTION_INTER":
                        res += constraint.evaluate() / 2
                    else:
                        res += constraint.evaluate()
        return res

    def get_total_bead_volume(self):
        res = 0
        for chromosome in self.chromosomes:
            for bead in chromosome:
                res += (4 / 3) * np.pi * (bead.radius ** 3)
        return res

    def get_total_genome_length(self):
        res = 0
        for chromosome in self.chromosomes:
            for bead in chromosome:
                res += bead.get_bp_span()
        return res

    def rescale_bead_size(self, occupancy_factor):
        assert self.has_nucleus
        bead_volume = self.get_total_bead_volume()
        genome_length = self.get_total_genome_length()
        for chromosome in self.chromosomes:
            for bead in chromosome:
                if type(self.nucleus_radius) is float:
                    new_radius = (((occupancy_factor * bead.get_bp_span()) / genome_length) ** (
                                1 / 3)) * self.nucleus_radius
                    bead.set_radius(new_radius)
                elif type(self.nucleus_radius) is tuple:
                    new_radius = (((occupancy_factor * bead.get_bp_span()) / genome_length)
                                  ** (1 / 3)) * \
                                 ((self.nucleus_radius[0] * self.nucleus_radius[1] * self.nucleus_radius[2]) ** (1/3))
                    bead.set_radius(new_radius)
                else:
                    print(f"ERROR: cannot determine radius type: should be float or tuple, "
                          f"but got {self.nucleus_radius} instead")
                    sys.exit()

    def get_nuclear_occupancy(self):
        assert self.has_nucleus
        if type(self.nucleus_radius) is float:
            nuclear_volume = (4 / 3) * np.pi * (self.nucleus_radius ** 3)
        elif type(self.nucleus_radius) is tuple:
            nuclear_volume = (4 / 3) * np.pi * self.nucleus_radius[0] * self.nucleus_radius[1] * self.nucleus_radius[2]
        else:
            print(f"ERROR: cannot determine radius type: should be float or tuple, "
                  f"but got {self.nucleus_radius} instead")
            sys.exit()
        return self.get_total_bead_volume() / nuclear_volume

    @staticmethod
    def interaction_specified_symmetric(interaction_ids):
        sorted_interactions = sorted(interaction_ids)
        count_map = collections.Counter(sorted_interactions)
        if 1 in count_map.values():
            return False
        return True

    @staticmethod
    def interaction_weights_specified_symmetric(interaction_weights):
        for interaction in interaction_weights:
            new_pair = (interaction[1], interaction[0])
            if new_pair not in interaction_weights:
                return False
            else:
                if abs(interaction_weights[new_pair] - interaction_weights[interaction] > 0.000001):
                    return False
        return True

    def save_model(self, fp='model.pkl'):
        with open(fp, 'wb') as file:
            pickle.dump(self, file, pickle.HIGHEST_PROTOCOL)
    
    def load_model(self, fp):
        with open(fp, 'rb') as file:
            data = pickle.load(file)
        for parameter in data.__dict__:
            setattr(self, parameter, data.__dict__[parameter])
