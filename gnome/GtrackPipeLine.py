import os
import shutil
import glob
import tarfile


def sort_contacts_from_tar(tar_directory, specific_extraction_directory=None, inter=False, resolution='1mb', mapping='MAPQGE30'):
    tf = tarfile.open(tar_directory, mode="r")
    if specific_extraction_directory is None:
        extraction_directory = ''
    tf.extractall(members=extraction_directory)
    if inter is True:
        outdir = './inter_chr_RAWobserved'
    else:
        outdir = './intra_chr_RAWobserved'

    x = os.walk(extraction_directory)

    for root, dirs, files in x:
        for name in dirs:
            path_look = os.path.join(root, name)
            if path_look.split('/')[-1] == mapping:
                print(path_look)
                files_to_copy = glob.iglob(os.path.join(path_look, "*.RAWobserved"))
                for file in files_to_copy:
                    shutil.copy(file, outdir)


def check_nchg_in_environment():
    if os.environ.get('*NCHG*') is None:
        nchg_directory = input("NCHG is not found in the working environment. Please provide path: ")
        os.environ["NCHG"] = nchg_directory
