import numpy as np
from numba import njit
import random
from math import sqrt, hypot, copysign


@njit
def euclidean_distance(vector1: np.array, vector2: np.array):
    assert vector1.shape == vector2.shape
    return np.sqrt(np.sum((vector1 - vector2) ** 2))


@njit
def repeat_vector(vector: np.array, dim):
    mat = np.ones((dim, len(vector)))
    return mat * vector


@njit
def normalise_vector(vector: np.array):
    return vector / np.sqrt(np.sum(np.square(vector)))


@njit
def bead_coordinate_clash(array1, array2, radius, radius2):
    arr1_list = range(array1.shape[0])
    arr2_list = range(array2.shape[0])
    for m in arr1_list:
        for b in arr2_list:
            if (((array1[m, 0] - array2[b, 0]) ** 2) + ((array1[m, 1] - array2[b, 1]) ** 2) +
               ((array1[m, 2] - array2[b, 2]) ** 2)) < ((radius[m] + radius2[b]) ** 2)-1.0e-15:
                return True
    return False


@njit
def create_self_avoiding_walk(radii, origin, call_seed, max_iterations=1000):
    assert len(radii) > 0
    random.seed(call_seed)
    previous_radii = radii[0]
    previous_coords = origin
    x = 0
    y = 0
    z = 0
    # Current bug in numba where memory leak occurs for multi-dimensional array creation
    # This is corrected by making 1D array and then reshape it
    # Issue report number https://github.com/numba/numba/issues/4093
    new_coord_map = np.empty(len(radii)*3).reshape(-1, 3)
    new_coord_map[0] = origin
    j = 0
    for i, radius in enumerate(radii):
        if i == 0:
            continue
        r = radii[i] + previous_radii
        for iteration in range(0, max_iterations):
            theta = random.uniform(0, 2 * np.pi)
            z = random.uniform(-r, r)
            x = sqrt(((r ** 2) - (z ** 2))) * np.cos(theta)
            y = sqrt((r ** 2) - (z ** 2)) * np.sin(theta)

            x += previous_coords[0]
            y += previous_coords[1]
            z += previous_coords[2]

            # Current bug in numba where memory leak occurs for multi-dimensional array creation
            # This is corrected by making 1D array and then reshape it
            # Issue report number https://github.com/numba/numba/issues/4093
            proposed_coords = np.array([x, y, z]).reshape(-1, 3)
            j += 1
            if not bead_coordinate_clash(proposed_coords, new_coord_map[:i], np.array([radius]), radii[:i]):
                break
            if iteration == max_iterations - 1:  # Was unable to find non-clashing structure
                return None
        new_coord_map[i] = [x, y, z]
        previous_coords = np.array([x, y, z])
        previous_radii = radius
    return new_coord_map


@njit
def rotate_points_arbitrary_axis(theta, start_point, end_point, coordinate_matrix):
    normalised_vector = normalise_vector(start_point - end_point)

    C = np.cos(theta)
    S = np.sin(theta)
    T = 1 - C
    ux = normalised_vector[0]
    uy = normalised_vector[1]
    uz = normalised_vector[2]

    rotation_matrix = np.empty((3, 3))
    rotation_matrix[0, 0] = T * ux * ux + C
    rotation_matrix[1, 0] = T * ux * uy + S * uz
    rotation_matrix[2, 0] = T * ux * uz - S * uy
    rotation_matrix[0, 1] = T * ux * uy - S * uz
    rotation_matrix[1, 1] = T * uy * uy + C
    rotation_matrix[2, 1] = T * uy * uz + S * ux
    rotation_matrix[0, 2] = T * ux * uz + S * uy
    rotation_matrix[1, 2] = T * uy * uz - S * ux
    rotation_matrix[2, 2] = T * uz * uz + C

    result = np.transpose(np.dot(rotation_matrix, np.transpose(coordinate_matrix-start_point)))

    return result + start_point


@njit
def to_numpy_array(array):
    h = len(array)
    w = len(array[0])
    new = np.empty((h, w))
    for i in range(h):
        for j in range(w):
            new[i, j] = array[i][j]
    return new


# def min_distance_point_and_ellipsoid(bead, a, b, c):
#     t = Symbol('t')
#     solutions = solve((((a**2)*(bead.x**2)/((a**2)-t)**2) +
#                        ((b**2)*(bead.y**2)/((b**2)-t)**2) +
#                        ((c**2)*(bead.z**2)/((c**2)-t)**2))-1, t)
#     chosen_solution = min(solutions)
#     ellipsoid_x = 0

def solve(semi_major, semi_minor, p):
    # https://stackoverflow.com/questions/22959698/distance-from-given-point-to-given-ellipse
    # https://wet-robots.ghost.io/simple-method-for-distance-to-ellipse/
    px = abs(p[0])
    py = abs(p[1])

    tx = 0.707
    ty = 0.707

    a = semi_major
    b = semi_minor

    for x in range(0, 3):
        x = a * tx
        y = b * ty

        ex = (a*a - b*b) * tx**3 / a
        ey = (b*b - a*a) * ty**3 / b

        rx = x - ex
        ry = y - ey

        qx = px - ex
        qy = py - ey

        r = hypot(ry, rx)
        q = hypot(qy, qx)

        tx = min(1, max(0, (qx * r / q + ex) / a))
        ty = min(1, max(0, (qy * r / q + ey) / b))
        t = hypot(ty, tx)
        tx /= t
        ty /= t

    return copysign(a * tx, p[0]), copysign(b * ty, p[1])


def check_in_nucleus(bead, a, b, c):
    # https://www.geeksforgeeks.org/check-if-a-point-is-inside-outside-or-on-the-ellipse/
    return ((bead.x ** 2) / (a ** 2) + (bead.y ** 2) / (b ** 2) + (bead.z ** 2) / (c ** 2))**0.5


