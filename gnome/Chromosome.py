import bisect

import numpy as np

from . import Bead, Util


class Chromosome(object):

    def __init__(self, chr_name: str):
        self.name = chr_name
        self.beadList = []

    def __str__(self):
        for bead in self.beadList:
            print(f"{bead}\n")

    def __repr__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return not self.__eq__(other)

    def __getitem__(self, item):
        return self.beadList[item]

    def __len__(self):
        return len(self.beadList)

    def add_bead(self, bead: Bead):

        assert bead.hasCoord
        pos = bisect.bisect_left(self.beadList, bead)
        assert (pos == len(self.beadList) or bead < pos)
        self.beadList.insert(pos, bead)

    def get_bead_coordinates(self, *args):
        if len(args) == 2:
            start_position = args[0]
            end_position = args[1]
            assert (start_position <= end_position)
            assert (end_position < len(self.beadList))
            bead_iter = iter(self.beadList[start_position: end_position+1])
            mat = np.empty(((end_position - start_position) + 1, 3))
            for i, bead in enumerate(bead_iter):
                mat[i, 0] = bead.x
                mat[i, 1] = bead.y
                mat[i, 2] = bead.z
            return mat
        elif len(args) == 1:
            position = args[0]
            mat = self.get_bead_coordinates(position, position)
            return mat[0]
        else:
            return self.get_bead_coordinates(0, len(self.beadList)-1)

    def set_colour(self, red: float, green: float, blue: float):
        assert (0 <= red <= 1)
        assert (0 <= green <= 1)
        assert (0 <= blue <= 1)

        for bead in self.beadList:
            bead.set_colour(red, green, blue)

    def crankshaft(self, start_position: int, end_position: int, theta: float):
        start_point = self.get_bead_coordinates(start_position)
        end_point = self.get_bead_coordinates(end_position)
        mat = self.get_bead_coordinates(start_position + 1, end_position - 1)
        rotated = Util.rotate_points_arbitrary_axis(theta, start_point, end_point, mat)

        return rotated

    def rotation(self, start_position: int, end_position: int, theta: float):
        start_point = self.get_bead_coordinates(start_position)
        end_point = self.get_bead_coordinates(end_position)
        mat = self.get_bead_coordinates(0, len(self.beadList)-1)
        rotated = Util.rotate_points_arbitrary_axis(theta, start_point, end_point, mat)
        return rotated

    def arm_rotate(self, position: int, theta: float, go_right: bool):
        assert (1 <= position < len(self.beadList) - 1)
        assert (0 <= theta <= 2 * np.pi)

        if go_right:
            mat = self.get_bead_coordinates(position + 1, len(self.beadList) - 1)
            point1 = self.get_bead_coordinates(position - 1)
            point2 = self.get_bead_coordinates(position)
        else:
            mat = self.get_bead_coordinates(0, position - 1)
            point1 = self.get_bead_coordinates(position)
            point2 = self.get_bead_coordinates(position + 1)

        rotated = Util.rotate_points_arbitrary_axis(theta, point1, point2, mat)

        return rotated

    def translate(self, translation):
        return self.get_bead_coordinates() + translation
