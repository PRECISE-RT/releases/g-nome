"""
Multimethod implementation from Guido's post [1] and brought to my attention by the following stack overflow post [2].
Whilst we could probably get away with not having explicit method type-based-overloading I think this is a good solution
currently. However this may change in future development.
[1] https://www.artima.com/weblogs/viewpost.jsp?thread=101605
[2] https://stackoverflow.com/questions/6434482/python-function-overloading
"""

registry = {}


class MultiMethod(object):
    def __init__(self, name):
        self.name = name
        self.typemap = {}

    def __call__(self, *args):
        types = tuple(arg.__class__ for arg in args)  # a generator expression!
        function = self.typemap.get(types)
        if function is None:
            raise TypeError("no match")
        return function(*args)

    def register(self, types, function):
        if types in self.typemap:
            raise TypeError("duplicate registration")
        self.typemap[types] = function


def multimethod(*types):
    def register(function):
        name = function.__name__
        mm = registry.get(name)
        if mm is None:
            mm = registry[name] = MultiMethod(name)
        mm.register(types, function)
        return mm

    return register
