from numpy cimport ndarray as ar
import numpy as np
cimport cython

@cython.boundscheck(False)
@cython.wraparound(False)
def toarray(xy):
    cdef int i, j, h=len(xy), w=len(xy[0])
    cdef ar[double,ndim=2] new = np.empty((h,w), dtype='float64')
    for i in range(h):
        for j in range(w):
            new[i,j] = xy[i][j]
    return new


@cython.boundscheck(False)
@cython.wraparound(False)
def bead_coordinate_clash(ar[double,ndim=2] array1, ar[double,ndim=2] array2,
                          ar[double,ndim=1] radius, ar[double,ndim=1] radius2):
    cdef int m, b
    for m in range(array1.shape[0]):
        for b in range(array2.shape[0]):
            # x = (pow((array1[m, 0] - array2[b, 0]),2) + pow((array1[m, 1] - array2[b, 1]), 2) +
            #         pow((array1[m, 2] - array2[b, 2]), 2))
            # y = pow((radius[m] + radius2[b]), 2)-1.0e-15
            # if decimal.Decimal(x) < decimal.Decimal(y):
            if (((array1[m, 0] - array2[b, 0])**2) + ((array1[m, 1] - array2[b, 1])**2) +
                ((array1[m, 2] - array2[b, 2])**2)) < ((radius[m] + radius2[b])**2)-1.0e-12:
                return True
    return False