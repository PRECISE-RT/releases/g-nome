from . import Bead, Util


class Constraint(object):
    def __init__(self, bead1: Bead, bead2: Bead, target_distance, constraint_type="Regular", spring_constant=1, lower_bound=0, upper_bound="inf"):

        self.bead1 = bead1
        self.bead2 = bead2
        self.k = spring_constant
        self.target_distance = target_distance
        self.constraint_type = constraint_type
        self.lower = lower_bound
        self.upper = upper_bound
        self.active = True

    def get_bead_ids(self):
        return self.bead1, self.bead2

    def contact_evaluate(self):
        bead_distance = self.bead1 - self.bead2

        if self.lower < bead_distance < float(self.upper):
            return self.k * (bead_distance - self.target_distance) ** 2
        else:
            return 0

    def nucleus_evaluate(self):
        if type(self.target_distance) is float:  # Spheroid
            geometric_distance = Util.check_in_nucleus(
                self.bead1,
                self.target_distance - self.bead1.radius,
                self.target_distance - self.bead1.radius,
                self.target_distance - self.bead1.radius,
            )
            if geometric_distance > 1:
                geometric_distance = (
                    (self.bead1.x ** 2) + (self.bead1.y ** 2) + (self.bead1.z ** 2)
                ) - (self.target_distance - self.bead1.radius) ** 2
                return self.k * geometric_distance ** 2
            else:
                return 0
        else:
            assert type(self.target_distance) is tuple  # Ellipsoid
            geometric_distance = Util.check_in_nucleus(
                self.bead1,
                self.target_distance[0] - self.bead1.radius,
                self.target_distance[1] - self.bead1.radius,
                self.target_distance[2] - self.bead1.radius,
            )
            if geometric_distance > 1:
                # TODO add more sophisticated method of deriving suitable cost objective (e.g. minimal shortest distance)
                return self.k * geometric_distance ** 2

            else:
                return 0

    def periphery_evaluate(self):
        if type(self.target_distance) is float:  # Spheroid
            geometric_distance = Util.check_in_nucleus(
                self.bead1,
                self.target_distance - self.bead1.radius,
                self.target_distance - self.bead1.radius,
                self.target_distance - self.bead1.radius,
            )
            if geometric_distance == 1:
                return 0
            else:
                if geometric_distance > 1:
                    geometric_distance = ((self.bead1.x ** 2) + (self.bead1.y ** 2) + (
                        self.bead1.z ** 2)) - ((self.target_distance - self.bead1.radius) ** 2)
                    return self.k * geometric_distance ** 2
                else:
                    geometric_distance = (
                        (self.target_distance - self.bead1.radius) ** 2) - ((self.bead1.x ** 2) + (self.bead1.y ** 2) + (self.bead1.z ** 2))
                    return self.k * geometric_distance ** 2

        else:
            assert type(self.target_distance) is tuple  # Ellipsoid
            geometric_distance = Util.check_in_nucleus(
                self.bead1,
                self.target_distance[0] - self.bead1.radius,
                self.target_distance[1] - self.bead1.radius,
                self.target_distance[2] - self.bead1.radius,
            )
            if geometric_distance == 1:
                return 0
            else:
                # TODO add more sophisticated method of deriving suitable cost objective (e.g. minimal shortest distance)
                return self.k * geometric_distance ** 2

    def evaluate(self):
        if self.constraint_type == "NUCLEUS":  # only set if used --ConstrainNucleus in main args
            return self.nucleus_evaluate()
        elif self.constraint_type == "PERIPHERY":  # only set if gtrack file has LADs
            return self.periphery_evaluate()
        else:  # interactions
            return self.contact_evaluate()
