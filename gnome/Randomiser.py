import numpy as np

from . import Chromosome
from . import Util


class Move(object):

    def __init__(self, chromosome=None, start=None, stop=None, mat=None):
        if chromosome is None:
            self.chromosome = Chromosome.Chromosome('chr')
        else:
            self.chromosome = chromosome
        self.start = start
        self.stop = stop
        self.mat = mat

    def validate(self):
        assert (self.start <= self.stop)
        assert self.mat.shape[0] == (self.stop - self.start) + 1
        assert self.mat.shape[1] == 3


class CrankShaftRandomiser(Move):

    def __init__(self, chromosome=None, start=None, stop=None, mat=None):
        Move.__init__(self, chromosome, start, stop, mat)

    def randomise(self, rng, chrom=None):
        if chrom is not None:
            self.chromosome = chrom
        r1 = rng.randint(0, len(self.chromosome) - 3)
        r2 = rng.randint(r1 + 2, len(self.chromosome) - 1)
        theta = rng.uniform(0.0, 2 * np.pi - 0.001)
        mat = self.chromosome.crankshaft(r1, r2, theta)

        mov = Move(chromosome=self.chromosome, start=r1+1, stop=r2-1, mat=mat)
        return mov


class MicroCrankShaftRandomiser(Move):

    def __init__(self, chromosome=None, start=None, stop=None, mat=None):
        Move.__init__(self, chromosome, start, stop, mat)

    def randomise(self, rng, chrom=None):
        if chrom is not None:
            self.chromosome = chrom
        r1 = rng.randint(0, len(self.chromosome) - 3)
        r2 = r1+2
        theta = rng.uniform(0.0, 2 * np.pi - 0.001)
        mat = self.chromosome.crankshaft(r1, r2, theta)

        mov = Move(chromosome=self.chromosome, start=r1 + 1, stop=r2 - 1, mat=mat)
        return mov


class TranslateRandomiser(Move):

    def __init__(self, max_move, chromosome=None, start=None, stop=None, mat=None):
        Move.__init__(self, chromosome, start, stop, mat)
        self.max_move = max_move

    def randomise(self, rng, chrom=None):
        if chrom is not None:
            self.chromosome = chrom
        coordinate_vector = list()
        coordinate_vector.append(rng.randint(-self.max_move, self.max_move))
        coordinate_vector.append(rng.randint(-self.max_move, self.max_move))
        coordinate_vector.append(rng.randint(-self.max_move, self.max_move))

        coordinate_matrix = self.chromosome.translate(coordinate_vector)
        mov = Move(chromosome=self.chromosome, start=0, stop=len(self.chromosome)-1, mat=coordinate_matrix)
        return mov


class ArmWiggleRandomiser(Move):

    def __init__(self, chromosome=None, start=None, stop=None, mat=None):
        Move.__init__(self, chromosome, start, stop, mat)

    def randomise(self, rng, chrom=None):
        if chrom is not None:
            self.chromosome = chrom
        r1 = rng.randint(1, len(self.chromosome) - 2)

        coordinate_vector = np.empty(3)
        coordinate_vector[0] = self.chromosome[r1].x
        coordinate_vector[1] = self.chromosome[r1].y
        coordinate_vector[2] = self.chromosome[r1].z

        go_right = rng.randint(0, 1)
        radii = []
        if go_right:
            for bead in self.chromosome[r1:]:
                radii.append(bead.radius)
            call_seed = rng.randint(0, 1e10)
            mat = Util.create_self_avoiding_walk(np.array(radii), coordinate_vector, call_seed)
            mov = Move(chromosome=self.chromosome, start=r1, stop=len(self.chromosome) - 1, mat=mat)
        else:
            for bead in self.chromosome[:r1]:
                radii.append(bead.radius)
            call_seed = rng.randint(0, 1e10)
            mat = Util.create_self_avoiding_walk(np.array(radii), coordinate_vector, call_seed)
            mov = Move(chromosome=self.chromosome, start=0, stop=r1-1, mat=mat)

        return mov


class ArmRotateRandomiser(Move):

    def __init__(self, chromosome=None, start=None, stop=None, mat=None):
        Move.__init__(self, chromosome, start, stop, mat)

    def randomise(self, rng, chrom=None):
        if chrom is not None:
            self.chromosome = chrom
        r1 = rng.randint(1, len(self.chromosome) - 2)
        go_right = rng.randint(0, 1)
        theta = rng.uniform(0.0, 2 * np.pi - 0.001)

        mat = self.chromosome.arm_rotate(r1, theta, go_right)

        if go_right:
            mov = Move(chromosome=self.chromosome, start=r1 + 1, stop=len(self.chromosome) - 1, mat=mat)
        else:
            mov = Move(chromosome=self.chromosome, start=0, stop=r1 - 1, mat=mat)

        return mov


class RotationRandomiser(Move):

    def __init__(self, chromosome=None, start=None, stop=None, mat=None):
        Move.__init__(self, chromosome, start, stop, mat)

    def randomise(self, rng, chrom=None):
        if chrom is not None:
            self.chromosome = chrom
        r1 = rng.randint(0, len(self.chromosome) - 2)
        r2 = rng.randint(r1 + 1, len(self.chromosome) - 1)
        theta = rng.uniform(0.0, 2 * np.pi - 0.001)

        mat = self.chromosome.rotation(r1, r2, theta)
        mov = Move(chromosome=self.chromosome, start=0, stop=len(self.chromosome) - 1, mat=mat)

        return mov
