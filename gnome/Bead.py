class Bead(object):
    __slots__ = ['start', 'end', 'radius', 'id', 'genomicPos', 'hasCoord', 'x', 'y', 'z', 'colour']

    def __init__(self):
        self.start = 0
        self.end = 0
        self.radius = 0
        self.id = "NA"
        self.genomicPos = 0
        self.hasCoord = False
        self.x = None
        self.y = None
        self.z = None
        self.colour = [0, 0, 0]

    def __repr__(self):
        # TODO Check this works, also think how to add coordinates also
        return f"Bead.initialize({self.start}, {self.end}, {self.radius}, {self.id}"

    def __sub__(self, other):
        return (((self.x - other.x) ** 2) + ((self.y - other.y) ** 2) + ((self.z - other.z) ** 2)) ** 0.5

    def __add__(self, other):
        return (((self.x + other.x) ** 2) + ((self.y + other.y) ** 2) + ((self.z + other.z) ** 2)) ** 0.5

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        return self.genomicPos < other.genomicPos

    def initialize(self, start_position: int, end_position: int, bead_radius: float, bead_id: str):
        assert (start_position < end_position)

        self.start = start_position
        self.end = end_position
        self.radius = bead_radius
        self.id = bead_id

        self.genomicPos = int((self.start + self.end) / 2)
        self.hasCoord = False

        self.colour = [1, 1, 0]

    def set_coordinates(self, x: float, y: float, z: float):
        self.x = x
        self.y = y
        self.z = z

        self.hasCoord = True

    def set_radius(self, set_radius: float):  # Should be able to replace with just setattr
        self.radius = set_radius

    def get_bp_span(self):
        return self.end - self.start

    def set_colour(self, red: float, green: float, blue: float):
        self.colour = [red, green, blue]
